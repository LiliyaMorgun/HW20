/*В папке calculator дана верстка макета калькулятора. Необходимо сделать этот калькулятор рабочим.
При клике на клавиши с цифрами - набор введенных цифр должен быть показан на табло калькулятора.
При клике на знаки операторов (`*`, `/`, `+`, `-`) на табло ничего не происходит - программа ждет введения второго числа для выполнения операции.
Если пользователь ввел одно число, выбрал оператор, и ввел второе число, то при нажатии как кнопки `=`, так и любого из операторов, в табло должен появиться результат выполенния предыдущего выражения.
При нажатии клавиш `M+` или `M-` в левой части табло необходимо показать маленькую букву `m` - это значит, что в памяти хранится число. Нажатие на `MRC` покажет число из памяти на экране. Повторное нажатие `MRC` должно очищать память.
*/


window.addEventListener("DOMContentLoaded",()=>{
    const btn = document.querySelector(".keys"),
    display = document.querySelector(".display > input")

    btn.addEventListener("click", function (e) {
        if (validate(/^[0-9.]$/, e.target.value)){
			calc.value1 += e.target.value
			show(calc.value1, display)
		};
		 if (validate(/^[*+-/]$/, e.target.value)){
			calc.sign = e.target.value
		};
		if (value1 !== "" && sign)
		if (validate(/^[0-9.]$/, e.target.value)){
			calc.value2 += e.target.value
			show(calc.value2, display)			
		};
					
		if(e.target.value == "C") {
            display.value = "";
            value1 = null;
            value2 = null;
            sign = null;
            res = null; 
        };

        if(e.target.value == "=") {
            
            switch (sign){
				case "*":
					res = +value1 * +value2;
					break;
				case "+" :
					res = +value1 + +value2;
					break;
				case "-":
					res = +value1 - +value2;
					break;            
				case "/":
					res = +value1 / +value2;
					break;            
            };
			
			show(calc.res, display)
        }	
    })
})

function validate (pattern, value) {
	return pattern.test(value)
}

const calc = {
    value1 : "",
    value2 : "",
	sign : "",
	res : "",	
}

function show (value, el) {
 el.value = value
}

